import { OmitType } from '@nestjs/swagger';
import { User } from 'src/users/entities/user.entity';

export class CreateAuthDto extends OmitType(User, [
  'id',
  'firstName',
  'lastName',
  'msisdn',
  'email',
  'confirmPassword',
  'createdAt',
  'updatedAt',
]) {}
