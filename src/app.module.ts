import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { BookingsModule } from './resources/bookings/bookings.module';
import { TrainsModule } from './resources/trains/trains.module';
import { TravelsModule } from './resources/travels/travels.module';
import { ItinerariesModule } from './resources/itineraries/itineraries.module';
import { NotificationMailerModule } from './resources/notifications/notification.module';

@Module({
  controllers: [],
  providers: [],
  imports: [
    UsersModule,
    AuthModule,
    BookingsModule,
    TrainsModule,
    TravelsModule,
    ItinerariesModule,
    NotificationMailerModule,
  ],
})
export class AppModule {}
