import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ClassSerializerInterceptor, Logger } from '@nestjs/common';
import { SERVER_PORT, SERVER_URL } from './constant';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: new Logger(),
  });

  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    next();
  });

  app.enableCors({
    allowedHeaders: '*',
    origin: '*',
  });

  app.useGlobalInterceptors(
    new ClassSerializerInterceptor(app.get(Reflector), {
      enableImplicitConversion: true,
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('Train ticket booking API Swagger')
    .setDescription(
      'Train ticket booking API Swagger: You can register and connect to the API to make ticket booking for a given journey on these APIs',
    )
    .setVersion('1.0')
    .addBearerAuth()
    .addServer(SERVER_URL)
    .build();

  const document = SwaggerModule.createDocument(app, config, {
    operationIdFactory: (controllerKey: string, methodKey: string) =>
      `${controllerKey.toLocaleLowerCase()}-${methodKey.toLowerCase()}`,
  });
  SwaggerModule.setup('swaggers', app, document, {
    swaggerOptions: {
      persistAuthorization: true,
    },
    customSiteTitle:
      'Train ticket booking Open API | Train ticket booking API for JWT',
  });

  await app.listen(SERVER_PORT);

  Logger.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
