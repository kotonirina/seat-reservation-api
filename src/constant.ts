import 'dotenv/config';

export const SERVER_PORT = process.env.SERVER_PORT ?? 3000;
export const SERVER_URL = process.env.SERVER_URL ?? 'http://localhost:3000';

export const SECRET_KEY = process.env.SECRET_KEY ?? 'secreKey';
export const EXPIRES_IN = process.env.EXPIRES_IN ?? '3600s';
export const SMTP_HOST = process.env.SMTP_HOST;
export const SMTP_PORT: number = process.env.SMTP_PORT as unknown as number;
export const SMTP_TLS = process.env.SMTP_TLS === 'yes' ? true : false;
export const SMTP_USERNAME = process.env.SMTP_USERNAME;
export const SMTP_PASSWORD = process.env.SMTP_PASSWORD;
export const SMTP_SENDER = process.env.SMTP_SENDER;
export const NODE_ENV = process.env.NODE_ENV;

export const jwtConstants = {
  secret: SECRET_KEY,
};
