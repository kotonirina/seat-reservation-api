import { ApiProperty } from '@nestjs/swagger';
import { Itinerary as ItineraryModel } from '@prisma/client';

export class Itinerary implements ItineraryModel {
  @ApiProperty({
    description: 'The itenerary id',
  })
  id: string;

  @ApiProperty({
    description: 'The itenerary departure',
  })
  departure: string;

  @ApiProperty({
    description: 'The itenerary destination',
  })
  destination: string;

  @ApiProperty({
    description: 'The itenerary travel_fee',
  })
  travelFee: number;
}
