import { OmitType } from '@nestjs/swagger';
import { Itinerary } from '../entities/itinerary.entity';

export class CreateItineraryDto extends OmitType(Itinerary, ['id']) {}
