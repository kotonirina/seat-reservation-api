import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { PageMetaDto } from 'src/utils/dtos/page-meta.dto';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { CreateItineraryDto } from './dto/create-itinerary.dto';
import { UpdateItineraryDto } from './dto/update-itinerary.dto';
import { Itinerary } from './entities/itinerary.entity';

@Injectable()
export class ItinerariesService {
  constructor(private prisma: PrismaService) {}

  async create(createItineraryDto: CreateItineraryDto) {
    return await this.prisma.itinerary.create({
      data: createItineraryDto,
    });
  }

  async findAll(pageOptionsDto: PageOptionsDto): Promise<PageDto<Itinerary>> {
    const itemCount = await this.prisma.itinerary.count();
    const entities = await this.prisma.itinerary.findMany({
      skip: pageOptionsDto.skip,
      take: Number(pageOptionsDto.take),
      orderBy: {
        travelFee: pageOptionsDto.order,
      } as any,
    });

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  findOne(id: string) {
    return this.prisma.itinerary.findUnique({ where: { id: id } });
  }

  async update(id: string, updateItineraryDto: UpdateItineraryDto) {
    return await this.prisma.itinerary.update({
      data: updateItineraryDto,
      where: {
        id: id,
      },
    });
  }

  async remove(id: string) {
    return await this.prisma.itinerary.delete({
      where: {
        id: id,
      },
    });
  }
}
