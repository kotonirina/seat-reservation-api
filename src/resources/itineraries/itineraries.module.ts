import { Module } from '@nestjs/common';
import { ItinerariesService } from './itineraries.service';
import { ItinerariesController } from './itineraries.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [ItinerariesController],
  providers: [ItinerariesService],
  imports: [PrismaModule],
})
export class ItinerariesModule {}
