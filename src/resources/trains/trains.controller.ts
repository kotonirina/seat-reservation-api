import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TrainsService } from './trains.service';
import { CreateTrainDto } from './dto/create-train.dto';
import { UpdateTrainDto } from './dto/update-train.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Train } from './entities/train.entity';
import { ApiPaginatedResponse } from 'src/utils/decorators/api-paginated-response.decorator';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiBearerAuth()
@Controller('trains')
@ApiTags('Train available to book')
@UseGuards(JwtAuthGuard)
export class TrainsController {
  constructor(private readonly trainsService: TrainsService) {}

  @Post()
  @ApiOperation({
    summary: 'Add a train',
  })
  @ApiResponse({ status: 201, type: Train })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 400, description: 'Bad Request, Invalid input' })
  create(@Body() createTrainDto: CreateTrainDto) {
    return this.trainsService.create(createTrainDto);
  }

  @Get()
  @ApiOperation({ summary: 'get all availables trains' })
  @ApiPaginatedResponse(Train)
  @ApiResponse({ status: 200, type: [Train] })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async findAll(
    @Query() pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<Train>> {
    return await this.trainsService.findAll(pageOptionsDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'find a train by id' })
  @ApiResponse({ status: 200, type: Train })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 404,
    description: 'Not Found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  findOne(@Param('id') id: string) {
    return this.trainsService.findOne(id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'update train info' })
  @ApiResponse({ status: 201, type: Train })
  @ApiResponse({
    status: 404,
    description: 'Not Found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  async update(
    @Param('id') id: string,
    @Body() updateTrainDto: UpdateTrainDto,
  ) {
    return await this.trainsService.update(id, updateTrainDto);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Remove train from available list',
  })
  @ApiResponse({ status: 200, type: Train }) // TODO: Type: Boolean ==> true when success deleted and false otherwise
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  async remove(@Param('id') id: string) {
    return await this.trainsService.remove(id);
  }
}
