import { OmitType } from '@nestjs/swagger';
import { Train } from '../entities/train.entity';

export class CreateTrainDto extends OmitType(Train, ['id']) {}
