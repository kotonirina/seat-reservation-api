import { Injectable, Logger } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { PageMetaDto } from 'src/utils/dtos/page-meta.dto';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { CreateTrainDto } from './dto/create-train.dto';
import { UpdateTrainDto } from './dto/update-train.dto';
import { Train } from './entities/train.entity';

@Injectable()
export class TrainsService {
  constructor(private prisma: PrismaService) {}

  async create(createTrainDto: CreateTrainDto) {
    return await this.prisma.train.create({
      data: createTrainDto,
    });
  }

  async findAll(pageOptionsDto: PageOptionsDto): Promise<PageDto<Train>> {
    const itemCount = await this.prisma.train.count();
    const entities = await this.prisma.train.findMany({
      skip: pageOptionsDto.skip,
      take: Number(pageOptionsDto.take),
      orderBy: {
        name: pageOptionsDto.order,
      } as any,
    });

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  findOne(id: string) {
    return this.prisma.train.findUnique({ where: { id: id } });
  }

  async update(id: string, updateTrainDto: UpdateTrainDto) {
    return await this.prisma.train.update({
      data: updateTrainDto,
      where: {
        id: id,
      },
    });
  }

  async remove(id: string) {
    return await this.prisma.train.delete({
      where: {
        id: id,
      },
    });
  }
}
