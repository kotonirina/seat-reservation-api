import { ApiProperty } from '@nestjs/swagger';
import { Train as TrainModel } from '@prisma/client';

export class Train implements TrainModel {
  @ApiProperty({
    description: 'The train id',
  })
  id: string;

  @ApiProperty({
    description: 'The train name',
  })
  name: string;

  @ApiProperty({
    description: 'The train trainNumber',
  })
  trainNumber: string;

  @ApiProperty({
    description: 'The train placesCount',
  })
  placesCount: number;

  @ApiProperty({
    description: 'The train seatsNumber',
  })
  seatsNumber: number[];
}
