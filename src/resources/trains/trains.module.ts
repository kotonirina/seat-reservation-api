import { Module } from '@nestjs/common';
import { TrainsService } from './trains.service';
import { TrainsController } from './trains.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [TrainsController],
  providers: [TrainsService],
  imports: [PrismaModule],
})
export class TrainsModule {}
