import { ApiProperty } from '@nestjs/swagger';
import { MailInterface } from 'src/utils/interfaces';

export class INotificationMailerDto implements MailInterface {
  @ApiProperty({
    description: 'The booking id',
  })
  bookingId: string;

  @ApiProperty({
    description: 'The mail from',
  })
  from?: string;
  @ApiProperty({
    description: 'The mail to',
  })
  to: string | string[];
  @ApiProperty({
    description: 'The mail cc',
  })
  cc?: string | string[];
  @ApiProperty({
    description: 'The mail bcc',
  })
  bcc?: string | string[];
  @ApiProperty({
    description: 'The mail subject',
  })
  subject: string;
  @ApiProperty({
    description: 'The mail text',
  })
  text?: string;
  @ApiProperty({
    description: 'The mail html',
  })
  html?: string;
}
