import { OmitType } from '@nestjs/swagger';
import { ConfirmationMailer } from '../entities/notification.entity';

export class NotificationDto extends OmitType(ConfirmationMailer, ['id']) {}
