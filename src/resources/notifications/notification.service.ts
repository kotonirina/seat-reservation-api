import { MailerService } from '@nest-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { MailInterface } from 'src/utils/interfaces';
import { NotificationDto } from './dto/notification.dto';

@Injectable()
export class NotificationMailerService {
  constructor(private readonly mailerService: MailerService) {}

  async storeNotificationFromMailer(notifOption: NotificationDto) {
    return await new PrismaService().notification.create({
      data: notifOption,
    });
  }

  sendMail(options: MailInterface): Promise<any> {
    return this.mailerService.sendMail({
      to: options.to,
      cc: options.cc,
      bcc: options.bcc,
      from: options.from,
      subject: options.subject,
      text: options.text,
      html: options.html,
    });
  }
}
