import { ApiProperty } from '@nestjs/swagger';
import { Notification as NotificationModel } from '@prisma/client';

export class ConfirmationMailer implements NotificationModel {
  @ApiProperty({
    description: 'The Train Booking Notification id',
  })
  id: string;

  @ApiProperty({
    description: 'The Train Booking Notification bookingId',
  })
  bookingId: string;

  @ApiProperty({
    description: 'The Train Booking Notification subject',
  })
  subject: string;

  @ApiProperty({
    description: 'The Train Booking Notification message',
  })
  message: string;

  @ApiProperty({
    description: 'The Train Booking Notification status',
  })
  status: string;

  @ApiProperty({
    description: 'The Train Booking Notification createdAt',
  })
  createdAt: Date;
}
