import {
  Body,
  Controller,
  Logger,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { NotificationMailerService } from './notification.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ConfirmationMailer } from './entities/notification.entity';
import { INotificationMailerDto } from './dto/confirmation-mailer-response.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiBearerAuth()
@Controller('sendConfirmationMailer')
@ApiTags('Booking Confirmation With Notification mailer')
export class NotificationMailerController {
  constructor(
    private readonly notificationNailerService: NotificationMailerService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({
    summary: 'Send notification mailer',
  })
  @ApiResponse({ status: 201, type: ConfirmationMailer })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 400, description: 'Bad Request, Invalid input' })
  async sendMail(
    @Param('requestId') requestId: string | number | string[],
    @Body() mailOptions: INotificationMailerDto,
  ) {
    Logger.log('Sending email testing here ....');
    let status: string;
    await this.notificationNailerService
      .sendMail(mailOptions)
      .then(async (info) => {
        status = 'sent';

        Logger.log(`${requestId} - Mail sent successfully`);
        Logger.log(
          `${requestId} - [MailResponse]=${info.response} [MessageID]=${info.messageId}`,
        );
      })
      .catch((err) => {
        status = 'failed';
        Logger.error(`Problem with mail sending: ${err}`);
      });

    return await this.notificationNailerService.storeNotificationFromMailer({
      bookingId: mailOptions.bookingId,
      subject: mailOptions.subject,
      message: mailOptions.text || mailOptions.html || '',
      status: status,
      createdAt: new Date(),
    });
  }
}
