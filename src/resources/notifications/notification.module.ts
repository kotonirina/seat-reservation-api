import { MailerModule } from '@nest-modules/mailer';
import { Module } from '@nestjs/common';
import {
  SMTP_HOST,
  SMTP_PASSWORD,
  SMTP_PORT,
  SMTP_TLS,
  SMTP_USERNAME,
} from 'src/constant';
import { PrismaModule } from 'src/prisma/prisma.module';
import { NotificationMailerController } from './notification.controller';
import { NotificationMailerService } from './notification.service';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: SMTP_HOST,
        port: SMTP_PORT,
        secure: SMTP_TLS,
        auth: {
          user: SMTP_USERNAME,
          pass: SMTP_PASSWORD,
        },
      },
    }),
    PrismaModule,
  ],
  controllers: [NotificationMailerController],
  providers: [NotificationMailerService],
})
export class NotificationMailerModule {}
