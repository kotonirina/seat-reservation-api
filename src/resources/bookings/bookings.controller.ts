import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  NotFoundException,
  Logger,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ApiPaginatedResponse } from 'src/utils/decorators/api-paginated-response.decorator';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { BookingsService } from './bookings.service';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { Booking } from './entities/booking.entity';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiBearerAuth()
@ApiTags('Train Bookings')
@Controller('bookings')
@UseGuards(JwtAuthGuard)
export class BookingsController {
  constructor(private readonly bookingsService: BookingsService) {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({
    summary: 'Book one trip',
  })
  @ApiResponse({ status: 201, type: Booking })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 400, description: 'Bad Request, Invalid input' })
  async create(@Body() createBookingDto: CreateBookingDto) {
    return await this.bookingsService.create(createBookingDto);
  }

  @Get()
  @ApiOperation({ summary: 'get all trains Bookings' })
  @ApiPaginatedResponse(Booking)
  @ApiResponse({ status: 200, type: [Booking] })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async findAll(
    @Query() pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<Booking>> {
    return await this.bookingsService.findAll(pageOptionsDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'find one train Booking by id' })
  @ApiResponse({ status: 200, type: Booking })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 404,
    description: 'Not Found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async findOne(@Param('id') id: string) {
    const booking = await this.bookingsService.findOne(id);
    if (!booking) {
      throw new NotFoundException(id);
    }
    return booking;
  }

  // USE THIS API RESOURCE AS CANCELLED TRAIN BOOING RESERVATION
  @Patch(':id')
  @ApiOperation({
    summary:
      'update train Booking info ===> i.e To Cancel train Booking reservation | Change train booking reserved',
  })
  @ApiResponse({ status: 201, type: Booking })
  @ApiResponse({
    status: 404,
    description: 'Not Found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  async update(
    @Param('id') id: string,
    @Body() updateBookingDto: UpdateBookingDto,
  ) {
    return await this.bookingsService.update(id, updateBookingDto);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete booking trip',
  })
  @ApiResponse({ status: 200, type: Booking }) // TODO: Type: Boolean ==> true when success deleted and false otherwise
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  async remove(@Param('id') id: string) {
    return await this.bookingsService.remove(id);
  }
}
