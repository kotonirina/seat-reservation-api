import { OmitType } from '@nestjs/swagger';
import { Booking } from '../entities/booking.entity';

export class CreateBookingDto extends OmitType(Booking, ['id']) {
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
}
