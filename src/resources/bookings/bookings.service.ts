import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { PageMetaDto } from 'src/utils/dtos/page-meta.dto';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { Booking } from './entities/booking.entity';

@Injectable()
export class BookingsService {
  constructor(private prisma: PrismaService) {}

  async create(createBookingDto: CreateBookingDto) {
    return await this.prisma.booking.create({
      data: createBookingDto,
    });
  }

  async findAll(pageOptionsDto: PageOptionsDto): Promise<PageDto<Booking>> {
    const itemCount = await this.prisma.booking.count();
    const entities = await this.prisma.booking.findMany({
      skip: pageOptionsDto.skip,
      take: Number(pageOptionsDto.take),
      orderBy: {
        reservedAt: pageOptionsDto.order,
      } as any,
    });

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  findOne(id: string) {
    return this.prisma.booking.findUnique({ where: { id: id } });
  }

  async update(id: string, updateBookingDto: UpdateBookingDto) {
    return await this.prisma.booking.update({
      data: updateBookingDto,
      where: {
        id: id,
      },
    });
  }

  async remove(id: string) {
    return await this.prisma.booking.delete({
      where: {
        id: id,
      },
    });
  }
}
