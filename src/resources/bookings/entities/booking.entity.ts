import { ApiProperty } from '@nestjs/swagger';
import { Booking as BookingModel } from '@prisma/client';

export class Booking implements BookingModel {
  @ApiProperty({
    description: 'The booking id',
  })
  id: string;

  @ApiProperty({
    description: 'The booking userId',
  })
  userId: string;

  @ApiProperty({
    description: 'The booking itineraryId',
  })
  itineraryId: string;

  @ApiProperty({
    description: 'The booking travelTimeId',
  })
  travelTimeId: string;

  @ApiProperty({
    description: 'The booking placeNumber',
  })
  placeNumber: number[];

  @ApiProperty({
    description: 'The booking reservedAt',
  })
  reservedAt: Date;

  @ApiProperty({
    description: 'The booking status',
  })
  status: string;

  @ApiProperty({
    description: 'The booking tripCosts',
  })
  tripCosts: number;

  @ApiProperty({
    description: 'The booking createdAt',
  })
  createdAt: Date;

  @ApiProperty({
    description: 'The booking updatedAt',
  })
  updatedAt: Date;
}
