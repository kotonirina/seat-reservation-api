import { ApiProperty } from '@nestjs/swagger';
import { TravelTime as TravelTimeModel } from '@prisma/client';

export class Travel implements TravelTimeModel {
  @ApiProperty({
    description: 'The travel time id',
  })
  id: string;

  @ApiProperty({
    description: 'The travel time trainId',
  })
  trainId: string;

  @ApiProperty({
    description: 'The travel time date',
  })
  date: Date;

  @ApiProperty({
    description: 'The travel time status',
  })
  status: string;
}
