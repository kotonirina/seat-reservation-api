import { OmitType } from '@nestjs/swagger';
import { Travel } from '../entities/travel.entity';

export class CreateTravelDto extends OmitType(Travel, ['id']) {}
