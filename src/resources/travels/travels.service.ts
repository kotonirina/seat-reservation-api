import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { PageMetaDto } from 'src/utils/dtos/page-meta.dto';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { CreateTravelDto } from './dto/create-travel.dto';
import { UpdateTravelDto } from './dto/update-travel.dto';
import { Travel } from './entities/travel.entity';

@Injectable()
export class TravelsService {
  constructor(private prisma: PrismaService) {}

  async create(createTravelDto: CreateTravelDto) {
    return await this.prisma.travelTime.create({
      data: createTravelDto,
    });
  }

  async findAll(pageOptionsDto: PageOptionsDto): Promise<PageDto<Travel>> {
    const itemCount = await this.prisma.travelTime.count();
    const entities = await this.prisma.travelTime.findMany({
      skip: pageOptionsDto.skip,
      take: Number(pageOptionsDto.take),
      orderBy: {
        date: pageOptionsDto.order,
      } as any,
    });

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  findOne(id: string) {
    return this.prisma.travelTime.findUnique({ where: { id: id } });
  }

  async update(id: string, updateTravelDto: UpdateTravelDto) {
    return await this.prisma.travelTime.update({
      data: updateTravelDto,
      where: {
        id: id,
      },
    });
  }

  async remove(id: string) {
    return await this.prisma.travelTime.delete({
      where: {
        id: id,
      },
    });
  }
}
