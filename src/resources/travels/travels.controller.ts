import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TravelsService } from './travels.service';
import { CreateTravelDto } from './dto/create-travel.dto';
import { UpdateTravelDto } from './dto/update-travel.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Travel } from './entities/travel.entity';
import { ApiPaginatedResponse } from 'src/utils/decorators/api-paginated-response.decorator';
import { PageOptionsDto } from 'src/utils/dtos/page-options.dto';
import { PageDto } from 'src/utils/dtos/page.dto';
import { User } from 'src/users/entities/user.entity';
import { AuthenticatedUser } from 'src/common/decorators/authenticated-user.decorator';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiBearerAuth()
@Controller('travels')
@ApiTags('Travel Time for Train bookings')
@UseGuards(JwtAuthGuard)
export class TravelsController {
  constructor(private readonly travelsService: TravelsService) {}

  @Post()
  @ApiOperation({
    summary: 'Add a travel time',
  })
  @ApiResponse({ status: 201, type: Travel })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 400, description: 'Bad Request, Invalid input' })
  create(
    @AuthenticatedUser() user: User,
    @Body() createTravelDto: CreateTravelDto,
  ) {
    return this.travelsService.create(createTravelDto);
  }

  @Get()
  @ApiOperation({ summary: 'get all availables train bookings travel times' })
  @ApiPaginatedResponse(Travel)
  @ApiResponse({ status: 200, type: [Travel] })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async findAll(
    @Query() pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<Travel>> {
    return await this.travelsService.findAll(pageOptionsDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'find a travel time by id' })
  @ApiResponse({ status: 200, type: Travel })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 404,
    description: 'Not Found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  findOne(@Param('id') id: string) {
    return this.travelsService.findOne(id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'update Travel time details' })
  @ApiResponse({ status: 201, type: Travel })
  @ApiResponse({
    status: 404,
    description: 'Not Found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  async update(
    @Param('id') id: string,
    @Body() updateTravelDto: UpdateTravelDto,
  ) {
    return await this.travelsService.update(id, updateTravelDto);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete travel time from available list',
  })
  @ApiResponse({ status: 200, type: Travel }) // TODO: Type: Boolean ==> true when success deleted and false otherwise
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  async remove(@Param('id') id: string) {
    return await this.travelsService.remove(id);
  }
}
