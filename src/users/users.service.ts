import { Injectable, Logger } from '@nestjs/common';
import { encryptUserPassword } from 'src/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(private prismaService: PrismaService) {}

  async create(createUserDto: CreateUserDto) {
    createUserDto.password = await encryptUserPassword(createUserDto.password);
    delete createUserDto.confirmPassword;
    return await this.prismaService.user.create({
      data: createUserDto,
    });
  }

  async findAll() {
    return await this.prismaService.user.findMany({
      select: {
        id: true,
        firstName: true,
        lastName: true,
        username: true,
        email: true,
        msisdn: true,
        createdAt: true,
        updatedAt: true,
        password: false,
      },
    });
  }

  async findOne(id: string) {
    return this.prismaService.user.findUnique({
      where: { id: id },
      select: {
        id: true,
        firstName: true,
        lastName: true,
        username: true,
        email: true,
        msisdn: true,
        createdAt: true,
        updatedAt: true,
        password: false,
      },
    });
  }

  // TODO: Need to review this method
  async findByUsename(username: string) {
    const users = await this.prismaService.user.findMany();

    const userResult = users.filter((user) => {
      return user.username === username;
    });

    return userResult[0];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
