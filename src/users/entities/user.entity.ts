import { ApiProperty } from '@nestjs/swagger';
import { User as UserModel } from '@prisma/client';
import {
  IsOptional,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Match } from 'src/common/match.decorator';

export class User implements UserModel {
  @ApiProperty({
    description: 'The user id',
  })
  id: string;

  @ApiProperty({
    description: 'The user firstName',
  })
  firstName: string;

  @ApiProperty({
    description: 'The user lastName',
  })
  @IsOptional()
  lastName: string | null;

  @ApiProperty({
    description: 'The user msisdn',
  })
  @IsOptional()
  msisdn: string | null;

  @ApiProperty({
    description: 'The username',
  })
  username: string;

  @ApiProperty({
    description: 'The user email',
  })
  email: string;

  @ApiProperty({
    description: 'The user password',
  })
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  password: string;

  @ApiProperty({
    description: 'The user confirm password',
  })
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @Match(User, (s) => s.password)
  confirmPassword: string;

  @ApiProperty({
    description: 'The user createdAt',
  })
  createdAt: Date;

  @ApiProperty({
    description: 'The user updatedAt',
  })
  updatedAt: Date;
}
