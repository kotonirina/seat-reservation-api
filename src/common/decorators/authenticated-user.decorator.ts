import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from 'src/users/entities/user.entity';

export const AuthenticatedUser = createParamDecorator(
  (_data: unknown, ctx: ExecutionContext): User => {
    // return new User(ctx.switchToHttp().getRequest().user);
    return new User();
  },
);
