import { ApiProperty } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';

export class ErrorResponse {
  @ApiProperty({
    description: 'The Internal Server Error Status Code',
  })
  statusCode: number = 500;

  @ApiProperty({
    description: 'The Internal Server Error Message',
  })
  message: string = 'Internal Server Error';
}

export const encryptUserPassword = async (
  password: string,
): Promise<string> => {
  const saltOrRounds = 10;
  const hashedPassword = await bcrypt.hash(password, saltOrRounds);

  return hashedPassword;
};
