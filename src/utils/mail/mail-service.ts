import { Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import {
  NODE_ENV,
  SMTP_HOST,
  SMTP_PASSWORD,
  SMTP_PORT,
  SMTP_SENDER,
  SMTP_TLS,
  SMTP_USERNAME,
} from 'src/constant';
import { MailInterface } from '../interfaces';

export default class MailService {
  private static instance: MailService;
  private transporter: nodemailer.Transporter;

  private constructor() {}
  static getInstance() {
    if (!MailService.instance) {
      MailService.instance = new MailService();
    }
    return MailService.instance;
  }
  async createLocalConnection() {
    let account = await nodemailer.createTestAccount();
    this.transporter = nodemailer.createTransport({
      host: account.smtp.host,
      port: account.smtp.port,
      secure: account.smtp.secure,
      auth: {
        user: account.user,
        pass: account.pass,
      },
    });
  }
  async createConnection() {
    this.transporter = nodemailer.createTransport({
      host: SMTP_HOST,
      port: SMTP_PORT,
      secure: SMTP_TLS,
      auth: {
        user: SMTP_USERNAME,
        pass: SMTP_PASSWORD,
      },
    });
  }

  async sendMail(
    requestId: string | number | string[],
    options: MailInterface,
  ) {
    return await this.transporter
      .sendMail({
        from: SMTP_SENDER || options.from,
        to: options.to,
        cc: options.cc,
        bcc: options.bcc,
        subject: options.subject,
        text: options.text,
        html: options.html,
      })
      .then((info) => {
        Logger.log(`${requestId} - Mail sent successfully`);
        Logger.log(
          `${requestId} - [MailResponse]=${info.response} [MessageID]=${info.messageId}`,
        );
        if (NODE_ENV === 'local') {
          Logger.log(
            `${requestId} - Nodemailer: ${nodemailer.getTestMessageUrl(info)}`,
          );
        }
        return info;
      })
      .catch((err) => Logger.log(`Problem sending email: ${err}`));
  }

  async verifyConnection() {
    return this.transporter.verify();
  }

  getTransporter() {
    return this.transporter;
  }
}
